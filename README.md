# RestService
## Introduction
This is a sample REST microservice which demonstrate the integration with keycloak server.
To bring the keycloak server up refer [keycloak](https://gitlab.com/scaffolding1/keycloak) project

This microservice has 2 endpoints `/hello/admin` and `/hello/user`. The `admin` endpoint is allowed only for the users 
who have admin role and `user` endpoint is allowed for both admin and user role.

## Keycloak Setup
  - Create realm in keycloak
  - Create new client in the realm - preferably name the client as your application but not mandatory
  - Enable the client and service accounts using the toggle button
  - Set the access type to confidential - Now you will be able to see Credentials tab
  - In the Credentials tab you will be able to see the client secret which will be used to generate the access tokens
  - Create roles in the Roles tab
    - admin - role for admin users
    - user - role for standard users
    > **_Note:_** Set the password for the users while creation and toggle temporary password to off
  - Create role for realm and map the client roles in Composite roles section
    - app-admin - map both admin and user roles from client
    - app-user - map only the user role
    > **_Note:_** The app prefix is used just for differentiation. You can choose any name
  - Create users
    - admin - an admin user
    - stduser - a standard user
  - Now in the Role Mappings tab assign the realm roles to the users
    > **_Note:_** Assign both app-user and app-admin roles for admin users and only app-user role to standard user
    
Now you are all set for the testing the application!

**Important Note:**
```
While creating the client for the realm, provide the IP of your machine in Root URL and Valid Redirect URIs 
as localhost will not work because the keycloak is running in docker container.
```

## How to Test

### Generate Token
  - URL - `http://localhost:8080/auth/realms/TestRealm/protocol/openid-connect/token`
  - Body - use url encoded data
    - grant_type = password
    - scope = openid
    - client_id = {client created during the keycloak setup}
    - client_secret = {client secret from the Credentials tab of the client}
    - username = {username created during the keycloak setup}
    - password = {password set for the user}

### Test Application
Below are the application configurations need to be set for keycloak under `application.properties`
  - keycloak.realm = {Realm name created during the keycloak setup}
  - keycloak.auth-server-url = http://localhost:8080/auth
  - keycloak.ssl-required = external
  - keycloak.resource = {client created during the keycloak setup}
  - keycloak.credentials.secret = {client secret from the Credential tab}
  - keycloak.use-resource-role-mappings = true
  - keycloak.bearer-only = true

> Please note the `server.port` is set to `8888` because the keycloak server will be running on port `8080`
> Please feel free to change it to your favourite port number!

You can use any of the testing tools - for e.g. postman  
**_Admin Endpoint:_**
> http://localhost:8080/hello/admin  
> In the Authorization tab, select `Bearer Token` and paste the token generated from the [previous step](https://gitlab.com/scaffolding1/restservice#generate-token)  
> Response from the URL is `Hello Admin` in case the token supplied is generated for admin user  
> Otherwise you will get `403 Forbidden` response as this endpoint is enabled only for admin user 

**_User Endpoint:_**
> http://localhost:8080/hello/user  
> In the Authorization tab, select `Bearer Token` and paste the token generated from the [previous step](https://gitlab.com/scaffolding1/restservice#generate-token)  
> Response from the URL is `Hello User`  
> As this endpoint is enabled for standard user, token for both admin and standard user will work
